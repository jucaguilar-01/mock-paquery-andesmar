package com.mock.paquery.util;

import com.mock.paquery.dto.*;
import com.mock.paquery.dto.andesmar.AndesmarShipmentResponseDto;
import com.mock.paquery.dto.andesmar.AndesmarShipmentStatusResponseDto;
import com.mock.paquery.dto.andesmar.AndesmarShipmentUpdateResponseDto;
import com.mock.paquery.dto.andesmar.CredentialTokenResponseDto;
import com.mock.paquery.dto.response.ShipmentCreateResponseDto;

public class BuildMock {

    public static ShipmentCreateResponseDto mockShipmentStandard(){

        ShipmentCreateResponseDto responseDto = new ShipmentCreateResponseDto();

        ClientDto clientDto = new ClientDto();
        clientDto.setCreationDate("2021-12-04");
        clientDto.setEmail("clientPaquery@gmail.com");
        clientDto.setId("4512");
        clientDto.setIdNumber("002");
        clientDto.setIdType("4");
        clientDto.setLastname("cliente generico");
        clientDto.setMobilePhone("+5491556783425");
        clientDto.setModificationDate("2021-12-04");
        clientDto.setName("cliente");

        responseDto.setClient(clientDto);
        responseDto.setCaption("cliente capita");
        responseDto.setDeclaredValue("500");
        responseDto.setDeliveryTerm("23");
        responseDto.setDetail("crear pedido normal");
        responseDto.setExternalCode("1H23");
        responseDto.setExternalTypeCode("2");
        responseDto.setOwnerId("009");
        responseDto.setOwnerType("persona");
        responseDto.setPackageSize("120 cm");
        responseDto.setPackageType("caja mediana");
        responseDto.setRateId("3fa85f64-5717-4562-b3fc-2c963f66afa6");
        responseDto.setCode("00934");
        responseDto.setCreationDate("2021-12-03");
        responseDto.setModificationDate("2021-12-03");

        RollContainerDto rollContainerDto = new RollContainerDto();

        rollContainerDto.setRollContainerPosition("039393X,3848448Y");
        rollContainerDto.setRollContainerStatus("en camino");

        responseDto.setRollContainer(rollContainerDto);

        AddressDto addresSender = new AddressDto();
        addresSender.setComment("Piso 2 Dpto. A");
        addresSender.setGeolocationSource("googlemaps");
        addresSender.setGeolocationType("RANGE_INTERPOLATED");
        addresSender.setLatitude("-34.104390");
        addresSender.setLongitude("-54.104210");
        addresSender.setStreetName("Zapiola");
        addresSender.setStreetNumber("50400");
        addresSender.setW3wDescriptor("ISO-124");
        addresSender.setZoneId("caba_norte");

        responseDto.setSenderAddress(addresSender);


        AddressDto originAddress = new AddressDto();
        addresSender.setComment("Piso 4 Dpto. A");
        addresSender.setGeolocationSource("googlemaps");
        addresSender.setGeolocationType("RANGE_INTERPOLATED");
        addresSender.setLatitude("-35.104390");
        addresSender.setLongitude("-14.104210");
        addresSender.setStreetName("san martin");
        addresSender.setStreetNumber("4003");
        addresSender.setW3wDescriptor("ISO-124");
        addresSender.setZoneId("caba_norte");

        responseDto.setShippingAddress(originAddress);

        responseDto.setStatus(0);
        responseDto.setTrackingCode("457484");

        return responseDto;
    }

    public static PackageDeliverResponseDto mockPackage(){

        PackageDeliverResponseDto responseDto = new PackageDeliverResponseDto();
        responseDto.setAttachedDocuments(12981912123L);
        responseDto.setComments("envio normal");
        responseDto.setDeliveryTimestamp("2021-06-12T07:31:02Z");
        responseDto.setPaquerId("12981912123");
        responseDto.setTrackingNumber("110200AQ");

        return responseDto;

    }

    public static PackagePickupResponseDto mockPackagePickUp(){

        PackagePickupResponseDto responseDto = new PackagePickupResponseDto();
        responseDto.setComments("envio normal");
        responseDto.setPickerId("12981912123");
        responseDto.setTrackingNumber("110200AQ");

        return responseDto;

    }

    public static AndesmarShipmentResponseDto mockAndesmarShipment(){
        AndesmarShipmentResponseDto andesmarShipmentResponseDto = new AndesmarShipmentResponseDto();
        andesmarShipmentResponseDto.setCodigo("10");
        andesmarShipmentResponseDto.setNumeroPedido("1");
        andesmarShipmentResponseDto.setError("Guardada");
        return andesmarShipmentResponseDto;
    }

    public static AndesmarShipmentUpdateResponseDto mockAndesmarUpdate(){
        AndesmarShipmentUpdateResponseDto andesmarShipmentUpdateResponseDto = new AndesmarShipmentUpdateResponseDto();
        andesmarShipmentUpdateResponseDto.setOrderNUmber("12");
        andesmarShipmentUpdateResponseDto.setError("guardado");
        andesmarShipmentUpdateResponseDto.setDescripción("pedido bien");
        return andesmarShipmentUpdateResponseDto;
    }

    public static AndesmarShipmentStatusResponseDto mockAndesmarShipmentStatus(){
        AndesmarShipmentStatusResponseDto andesmarShipmentStatusResponseDto = new AndesmarShipmentStatusResponseDto();
        andesmarShipmentStatusResponseDto.setNumeroGuia(12.34);
        andesmarShipmentStatusResponseDto.setUnidadVentaDescripcion("unidad");
        andesmarShipmentStatusResponseDto.setModalidadEntregaDescripion("entregdo");
        andesmarShipmentStatusResponseDto.setEstadoActual("en camino");
        andesmarShipmentStatusResponseDto.setNumeroRemitoCliente("234");
        andesmarShipmentStatusResponseDto.setFechaEmision("2021-09-29T09:49:21.5154525-03:00");
        andesmarShipmentStatusResponseDto.setOrigen("38");
        andesmarShipmentStatusResponseDto.setNumeroPedido("2344");
        andesmarShipmentStatusResponseDto.setDestino("calle falsa 123");
        return andesmarShipmentStatusResponseDto;
    }

    public static CredentialTokenResponseDto mockCredentialToken() {
        CredentialTokenResponseDto credentialTokenResponseDto = new CredentialTokenResponseDto();
        credentialTokenResponseDto.setStatus("200");
        credentialTokenResponseDto.setUsuario("homero");
        credentialTokenResponseDto.setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IlNDQTYyNzIiLCJuYmYiOjE2MzE4MzUwMjEsImV4cCI6MTYzMTgzNjgyMSwiaWF0IjoxNjMxODM1MDIxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjQ5MjIwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0OTIyMCJ9.uj2D6U7V6uIhUhqy44NFAzjVzIbhEoIeHCzKEhvKRjMddwwqw");
    return credentialTokenResponseDto;
    }
}
