package com.mock.paquery.controller;

import com.mock.paquery.dto.andesmar.*;
import com.mock.paquery.util.BuildMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mock/andesmar")
@Slf4j
public class AndesmarMock {

    @PostMapping(path = "/insertPedido", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AndesmarShipmentResponseDto> andesMarInserpedido(@RequestBody AndesmarShipmentRequestDto andesmarShipmentStandard ) {
        log.info("service [insertPedido] data {}",andesmarShipmentStandard);
        return new ResponseEntity<>(BuildMock.mockAndesmarShipment(), HttpStatus.OK);
    }

    @PutMapping(path = "/updatePedidos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AndesmarShipmentUpdateResponseDto> andesMarUpdate(@RequestBody AndesmarShipmentUpdateRequestDto andesmarShipmentUpdate) {
        log.info("service [updatePedidos] data {}",andesmarShipmentUpdate);
        return new ResponseEntity<>(BuildMock.mockAndesmarUpdate(),HttpStatus.OK);
    }

    @PostMapping(path = "/EstadoActual", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AndesmarShipmentStatusResponseDto> andesStatus(@RequestBody AndesmarShipmentStatusRequestDto andesmarShipmentStatus) {
        log.info("service [EstadoActual] data {}",andesmarShipmentStatus);
        return new ResponseEntity<>(BuildMock.mockAndesmarShipmentStatus(),HttpStatus.OK);
    }

    @PostMapping(path = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialTokenResponseDto> andesStatus(@RequestBody CredentialTokenDto credentialTokenDto ) {
        log.info("service [authenticate] data {}",credentialTokenDto);
        CredentialTokenResponseDto responseDto = BuildMock.mockCredentialToken();
        log.info("exit service [authenticate] with body {}", responseDto);
        return new ResponseEntity<>(responseDto,HttpStatus.OK);
    }
}
