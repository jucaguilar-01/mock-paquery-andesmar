package com.mock.paquery.controller;

import com.mock.paquery.dto.*;
import com.mock.paquery.dto.response.ShipmentCreateResponseDto;
import com.mock.paquery.util.BuildMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mock/paquery")
@Slf4j
public class PaqueryMock {

    @PutMapping(path = "/shipments/manage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> shipmentManage(@RequestBody ShipmentManageRequestDto request) {
        log.info("service [shipmentManage] data {}", request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/shipments/standard", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShipmentCreateResponseDto> shipmentStandard(@RequestBody PaqueryShipmentStandardRequestDto request) {
        log.info("service [shipmentStandard] data {}", request);
        return new ResponseEntity<>(BuildMock.mockShipmentStandard(),HttpStatus.OK);
    }

    @PostMapping(path = "/shipments/withResolvedAddresses", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShipmentCreateResponseDto> shipmentWithResolvedAddresses(@RequestBody PaqueryShipmentWithAddressRequestDto request) {
        log.info("service [shipmentWithResolvedAddresses] data {}", request);
        return new ResponseEntity<>(BuildMock.mockShipmentStandard(),HttpStatus.OK);
    }

    @PutMapping(path = "/shipments/{shipmentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShipmentCreateResponseDto> shipmentUpdate(@PathVariable String shipmentId, @RequestBody PaqueryShipmentUpdateRequestDto request) {
        log.info("service [shipmentUpdate] data {} shipmentId {}", request, shipmentId);
        return new ResponseEntity<>(BuildMock.mockShipmentStandard(),HttpStatus.OK);
    }

    @GetMapping(path = "/shipments/{shipmentId}/history", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> getShipmentHistory(@PathVariable String shipmentId) {
        log.info("service [getShipmentHistory] shipmentId {}", shipmentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/shipments/{shipmentId}/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> getShipmentStatus(@PathVariable String shipmentId) {
        log.info("service [getShipmentStatus] shipmentId {}", shipmentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/shipments/{shipmentId}/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateShipmentStatus(@PathVariable String shipmentId, @RequestBody PaqueryShipmentStatusRequestDto requestDto) {
        log.info("service [updateShipmentStatus] shipmentId {} data {}", shipmentId, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/shipments/packages/deliverPackage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PackageDeliverResponseDto> createDeliverPackage(@RequestBody PackageDeliverDto packageDeliverDto) {
        log.info("service [createDeliverPackage] data {}", packageDeliverDto);
        return new ResponseEntity<>(BuildMock.mockPackage(),HttpStatus.OK);
    }

    @PostMapping(path = "/shipments/packages/pickup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PackagePickupResponseDto> createPickupPackage(@RequestBody PackagePickupDto packagePickupDto) {
        log.info("service [createPickupPackage] data {}", packagePickupDto);
        return new ResponseEntity<>(BuildMock.mockPackagePickUp(),HttpStatus.OK);
    }

    @GetMapping(path = "/shipments/packages/trackingNumber/{trackingNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> getPackageByTrackingId(@PathVariable String trackingNumber) {
        log.info("service [getPackageByTrackingId] trackingNumber {}", trackingNumber);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
