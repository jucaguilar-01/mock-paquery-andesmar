package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShipmentManageRequestDto {

    private String caption;
    private ClientDto client;
    private String code;
    @JsonProperty("declared_value")
    private Integer declaredValue;
    @JsonProperty("delivery_term")
    private String deliveryTerm;
    private String detail;
    @JsonProperty("externalCode")
    private String external_code;
    @JsonProperty("external_type_code")
    private String externalTypeCode;
    @JsonProperty("owner_id")
    private String ownerId;
    @JsonProperty("owner_type")
    private String ownerType;
    @JsonProperty("package_size")
    private String packageSize;
    @JsonProperty("package_type")
    private String packageType;
    @JsonProperty("rate_id")
    private String rateId;

}
