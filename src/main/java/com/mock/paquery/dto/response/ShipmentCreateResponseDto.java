package com.mock.paquery.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mock.paquery.dto.AddressDto;
import com.mock.paquery.dto.ClientDto;
import com.mock.paquery.dto.RollContainerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShipmentCreateResponseDto {

    private String caption;
    private ClientDto client;
    @JsonProperty("declared_value")
    private String declaredValue;
    @JsonProperty("delivery_term")
    private String deliveryTerm;
    private String detail;
    @JsonProperty("external_code")
    private String externalCode;
    @JsonProperty("external_type_code")
    private String externalTypeCode;
    @JsonProperty("owner_id")
    private String ownerId;
    @JsonProperty("owner_type")
    private String ownerType;
    @JsonProperty("package_size")
    private String packageSize;
    @JsonProperty("package_type")
    private String packageType;
    @JsonProperty("rate_id")
    private String rateId;
    private String code;
    private String creationDate;
    private String modificationDate;
    private RollContainerDto rollContainer;
    private AddressDto senderAddress;
    private AddressDto shippingAddress;
    private int status;
    private String trackingCode;

}
