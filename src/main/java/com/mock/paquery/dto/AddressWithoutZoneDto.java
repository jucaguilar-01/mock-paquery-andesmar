package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressWithoutZoneDto {

    private String comment;
    @JsonProperty("geolocation_source")
    private String geolocationSource;
    @JsonProperty("geolocation_type")
    private String geolocationType;
    private String latitude;
    private String longitude;
    @JsonProperty("street_name")
    private String streetName;
    @JsonProperty("w3w_descriptor")
    private String w3wDescriptor;
}
