package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    private String comment;
    @JsonProperty("geolocation_source")
    private String geolocationSource;
    @JsonProperty("geolocation_type")
    private String geolocationType;
    private String latitude;
    private String longitude;
    @JsonProperty("street_name")
    private String streetName;
    @JsonProperty("street_number")
    private String streetNumber;
    @JsonProperty("w3w_descriptor")
    private String w3wDescriptor;
    @JsonProperty("zone_id")
    private String zoneId;
}
