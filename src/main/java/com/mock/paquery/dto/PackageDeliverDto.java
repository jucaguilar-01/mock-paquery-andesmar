package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackageDeliverDto {

    @JsonProperty("attached_documents")
    private int attachedDocuments;
    private String comments;
    @JsonProperty("delivery_timestamp")
    private String deliveryTimestamp;
    @JsonProperty("paquer_id")
    private String paquerId;
    @JsonProperty("tracking_number")
    private String trackingNumber;
}