package com.mock.paquery.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackagePickupResponseDto {

    private String comments;
    private String pickerId;
    private String trackingNumber;

}
