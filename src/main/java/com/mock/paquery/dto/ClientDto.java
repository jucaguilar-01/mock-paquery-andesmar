package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {

    @JsonProperty("creation_date")
    private String creationDate;
    private String email;
    private String id;
    @JsonProperty("id_number")
    private String idNumber;
    @JsonProperty("id_type")
    private String idType;
    private String lastname;
    @JsonProperty("mobile_phone")
    private String mobilePhone;
    @JsonProperty("modification_date")
    private String modificationDate;
    private String name;
    private String phone;


}