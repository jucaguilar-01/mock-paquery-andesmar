package com.mock.paquery.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaqueryShipmentStatusRequestDto {

    private String newStatus;
    private String statusChangeInfo;

}
