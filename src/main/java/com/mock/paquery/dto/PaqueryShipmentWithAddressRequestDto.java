package com.mock.paquery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaqueryShipmentWithAddressRequestDto {

    private String caption;
    private ClientDto client;
    private String code;
    @JsonProperty("declared_value")
    private String declaredValue;
    @JsonProperty("delivery_term")
    private String deliveryTerm;
    private String detail;
    @JsonProperty("external_code")
    private String externalCode;
    @JsonProperty("external_type_code")
    private String externalTypeCode;
    @JsonProperty("owner_id")
    private String ownerId;
    @JsonProperty("owner_type")
    private String ownerType;
    @JsonProperty("package_size")
    private String packageSize;
    @JsonProperty("package_type")
    private String packageType;
    @JsonProperty("rate_id")
    private String rateId;
    @JsonProperty("resolved_sender_address")
    private AddressWithoutZoneDto resolvedSenderAddress;
    @JsonProperty("resolved_shipping_address")
    private AddressWithoutZoneDto resolvedShippingAddress;

}
