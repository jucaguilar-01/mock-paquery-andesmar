package com.mock.paquery.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RollContainerDto {

    private String rollContainerPosition;
    private String rollContainerStatus;
}
