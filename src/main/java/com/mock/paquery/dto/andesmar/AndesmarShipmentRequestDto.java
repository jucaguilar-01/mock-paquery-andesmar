package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentRequestDto {

    @JsonProperty("CodigoPostalRemitente")
    String codigoPostalRemitente;
    @JsonProperty("CalleRemitente")
    String calleRemitente;
    @JsonProperty("CalleNroRemitente")
    String calleNroRemitente;
    @JsonProperty("NombreApellidoDestinatario")
    String nombreApellidoDestinatario;
    @JsonProperty("CodigoPostalDestinatario")
    String codigoPostalDestinatario;
    @JsonProperty("CalleDestinatario")
    String calleDestinatario;
    @JsonProperty("CalleNroDestinatario")
    String CalleNroDestinatario;
    @JsonProperty("TelefonoDestinatario")
    String telefonoDestinatario;
    @JsonProperty("MailDestinatario")
    String mailDestinatario;
    @JsonProperty("NroRemito")
    String nroRemito;
    @JsonProperty("Bultos")
    Integer bultos;
    @JsonProperty("Peso")
    Double peso;
    @JsonProperty("ValorDeclarado")
    Integer valorDeclarado;
    @JsonProperty("M3")
    Double M3;
    @JsonProperty("Alto")
    Double Alto;
    @JsonProperty("Ancho")
    Double Ancho;
    @JsonProperty("Largo")
    Double largo;
    @JsonProperty("Observaciones")
    String observaciones;
    @JsonProperty("modalidadEntregaID")
    Integer ModalidadEntregaID;
    @JsonProperty("servicio")
    ServicioDto servicio;
    @JsonProperty("logueo")
    LogueoDto Logueo;
    @JsonProperty("CodigoAgrupacion")
    String codigoAgrupacion;
}
