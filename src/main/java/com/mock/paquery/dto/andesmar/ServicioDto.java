package com.mock.paquery.dto.andesmar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServicioDto {

    private Boolean esFletePagoDestino;
    private Boolean esRemitoconformado;
}
