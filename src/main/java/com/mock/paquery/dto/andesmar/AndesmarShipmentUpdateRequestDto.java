package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentUpdateRequestDto {

    @JsonProperty("logueo")
    private LogueoDto logueo;

    @JsonProperty("CalleRemitente")
    private String calleRemitente;

    @JsonProperty("CalleNroRemitente")
    private String calleNroRemitente;

    @JsonProperty("CalleDestinatario")
    private String calleDestinatario;

    @JsonProperty("CalleNroDestinatario")
    private String calleNroDestinatario;

    @JsonProperty("Pedido")
    private String numeroPedido;

    @JsonProperty("EsOrden")
    private Boolean esOrden;

    @JsonProperty("ValorDeclarado")
    private String valorDeclarado;

    @JsonProperty("TelefonoRemitente")
    private String telefonoRemitente;

    @JsonProperty("TelefonoDestinatario")
    private String telefonoDestinatario;
}
