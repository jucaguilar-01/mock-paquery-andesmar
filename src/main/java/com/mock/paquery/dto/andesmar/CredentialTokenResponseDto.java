package com.mock.paquery.dto.andesmar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredentialTokenResponseDto {

    String token;
    String usuario;
    String status;
}
