package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentStatusResponseDto {

    @JsonProperty("NroGuia")
    private Double numeroGuia;

    @JsonProperty("UnidadVentaDescrip")
    private String unidadVentaDescripcion;

    @JsonProperty("ModalidadEntregaDescrip")
    private String modalidadEntregaDescripion;

    @JsonProperty("EstadoActual")
    private String estadoActual;

    @JsonProperty("NroRemitoCliente")
    private String numeroRemitoCliente;

    @JsonProperty("FechaEmision")
    private String fechaEmision;

    @JsonProperty("Origen")
    private String origen;

    @JsonProperty("NroPedido")
    private String numeroPedido;

    @JsonProperty("Destino")
    private String destino;

    @JsonProperty("Error")
    private String error;

}
