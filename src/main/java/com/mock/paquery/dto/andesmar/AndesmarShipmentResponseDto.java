package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentResponseDto {

    @JsonProperty("nroPedido")
    private String numeroPedido;

    @JsonProperty("Codigo")
    private String codigo;

    @JsonProperty("Error")
    private String error;
}
