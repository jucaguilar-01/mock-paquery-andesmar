package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentStatusRequestDto {

    @JsonProperty("NroPedido")
    private String numeroPedido;

    @JsonProperty("logueo")
    private LogueoDto logueo;
}
