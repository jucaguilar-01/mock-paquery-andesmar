package com.mock.paquery.dto.andesmar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AndesmarShipmentUpdateResponseDto {

    @JsonProperty("Pedido")
    private String orderNUmber;

    @JsonProperty("codError")
    private String codError;

    @JsonProperty("Error")
    private String error;

    @JsonProperty("Descripcion")
    private String descripción;
}
