package com.mock.paquery.dto.andesmar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogueoDto {

    private String usuario;
    private String clave;
    private String codigoCliente;
}
