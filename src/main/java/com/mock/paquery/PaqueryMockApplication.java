package com.mock.paquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaqueryMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaqueryMockApplication.class, args);
	}

}
